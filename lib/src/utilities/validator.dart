import "dart:async";

class Validators {
  final validateId = StreamTransformer<String, String>.fromHandlers(
      handleData: (String id, EventSink<String> sink) {
    if (id.length >= 10)
      sink.add(id);
    else
      sink.addError('Enter a valid id');
  });
}

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:ogram/src/ui/home_ui.dart';
import 'package:ogram/src/ui/ogramupload.dart';

class UploadUI extends StatefulWidget {
  @override
  _UploadUI createState() => _UploadUI();
}

class _UploadUI extends State<UploadUI> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: HomeUI(),
      ),
      floatingActionButton: selectImageButton(context),
    );
  }

  selectImageButton(BuildContext mycontext) {
    return FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showMaterialModalBottomSheet(
            context: context,
            builder: (context, scrollController) => OgramUpload(),
          );
        });
  }
}

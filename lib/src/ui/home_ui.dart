import 'package:flutter/material.dart';
import 'package:ogram/src/blocs/home_bloc.dart';
import 'package:ogram/src/repository/dao/ogramfile_dao.dart';

class HomeUI extends StatelessWidget {
  HomeBloc _homeBloc = HomeBloc();
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    _homeBloc.init();
    homeBloc.getLisOgram();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 0)),
          Flexible(
              child: StreamBuilder(
                  stream: homeBloc.listOgram,
                  builder: (context, AsyncSnapshot<List<OgramFile>> snap) {
                    if (snap.hasData) {
                      return GridView.builder(
                          itemCount: snap.data == null ? 0 : snap.data.length,
                          gridDelegate:
                              new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onLongPress: () {},
                                child: Padding(
                                  padding: EdgeInsets.all(1),
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/load.gif',
                                    image: snap.data[index].link,
                                  ),
                                ));
                          });
                    } else {
                      return Center(
                        child: Text(
                          'NO OGRAM FILE',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'quicksand',
                              fontSize: 15),
                        ),
                      );
                    }
                  })),
        ],
      ),
    );
  }
}

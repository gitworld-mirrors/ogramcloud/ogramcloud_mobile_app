import 'package:flutter/material.dart';
import 'package:ogram/src/blocs/obtainId_bloc.dart';
import 'package:ogram/src/config/themes.dart';

class ObtainIdUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ObtainIdUIState();
}

class ObtainIdUIState extends State<ObtainIdUI> {
  ObtainIdBloc obtain = ObtainIdBloc();
  TextEditingController idController = new TextEditingController();
  final _keyform = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue,
      body: Center(
          child: Form(
              key: _keyform,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 200.0,
                    height: 150.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/cloud.png'))),
                  ),
                  Text(
                    'OGRAMCLOUD',
                    style: TextStyle(
                        color: AppColors.white,
                        fontFamily: 'baumans',
                        fontSize: AppSize.h1),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 30.0, 12.0, 10.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.blueAccent,
                      elevation: 0.0,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: StreamBuilder<String>(
                            stream: obtain.id,
                            builder: (context, snap) {
                              return TextFormField(
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                                decoration: InputDecoration(
                                    errorText: snap.error,
                                    fillColor: Colors.white,
                                    border: InputBorder.none,
                                    hintText: "ID OGRAM",
                                    hintStyle: TextStyle(
                                        fontFamily: 'quicksand',
                                        color: Colors.white),
                                    icon: Icon(
                                      Icons.cloud_off,
                                      color: Colors.white,
                                    )),
                                keyboardType: TextInputType.text,
                                onChanged: obtain.changeId,
                                controller: idController,
                                validator: (value) {
                                  if (value.length <= 9) {
                                    return "ID isn't valid";
                                  } else if (value.isEmpty) {
                                    return "ID is required";
                                  }
                                  return null;
                                },
                              );
                            }),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 100.0, right: 100.0, top: 15.0),
                    child: GestureDetector(
                      onTap: () {
                        if (_keyform.currentState.validate()) {}
                      },
                      child: Material(
                        borderRadius: BorderRadius.circular(100.0),
                        color: Color(0xffffffff),
                        elevation: 0.0,
                        child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: StreamBuilder<bool>(
                                stream: obtain.isSubmit,
                                builder: (context, snap) {
                                  return MaterialButton(
                                    minWidth: MediaQuery.of(context).size.width,
                                    onPressed:
                                        (!snap.hasData) ? null : obtain.submit,
                                    child: Text(
                                      "SAVE",
                                      style: TextStyle(
                                          fontSize: AppSize.textbutton,
                                          fontFamily: 'quicksand',
                                          color: Colors.blue),
                                    ),
                                  );
                                })),
                      ),
                    ),
                  ),
                  StreamBuilder<bool>(
                    stream: obtain.loading,
                    builder: (context, snap) {
                      return Container(
                        margin: EdgeInsets.only(top: 20.0),
                        child: (snap.hasData && snap.data)
                            ? CircularProgressIndicator(
                                backgroundColor: Colors.white)
                            : null,
                      );
                    },
                  )
                ],
              ))),
    );
  }
}

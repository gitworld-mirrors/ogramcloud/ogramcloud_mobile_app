import 'package:flutter/material.dart';
import 'package:ogram/src/resources/database/saveLocal.dart';
import 'package:ogram/src/ui/obtainId_ui.dart';
import 'package:ogram/src/ui/upload_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BodyMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => new MyHomePageState();
}

class MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  int _index;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 2, vsync: this);
    _index = 0;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(children: [
                Icon(Icons.cloud),
                Text(
                  ' OgramCloud',
                  style: TextStyle(fontFamily: 'quicksand'),
                )
              ]),
              Row(children: [
                FutureBuilder(
                    future: saveLocal.getStringId(),
                    builder: (context, snap) {
                      return (!snap.hasData)
                          ? Container(
                              height: 10,
                              width: 10,
                              child: CircularProgressIndicator(
                                  backgroundColor: Colors.white),
                            )
                          : Text(
                              'ID: ' + snap.data,
                              style: TextStyle(fontFamily: 'quicksand'),
                            );
                    })
              ])
            ],
          ),
          bottom: new TabBar(controller: _controller, tabs: <Tab>[
            new Tab(text: "OGRAM GALERY"),
            new Tab(text: "FOLDER")
          ]),
        ),
        body: new TabBarView(
          controller: _controller,
          // children: <Widget>[new NewPage(_index), new HotelsPage(_index)],
          children: <Widget>[UploadUI(), NewPage(_index)],
        ));
  }
}

class NewPage extends StatelessWidget {
  final int index;

  NewPage(this.index);

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text('Next Ogram Features ^_^'),
    );
  }
}

class HotelsPage extends StatelessWidget {
  final int index;

  HotelsPage(this.index);

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text('HotelsPage, index: $index'),
    );
  }
}

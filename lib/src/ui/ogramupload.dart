import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';
import 'package:ogram/src/blocs/home_bloc.dart';
import 'package:ogram/src/repository/dao/ogramfile_dao.dart';
import 'package:ogram/src/repository/database.dart';
import 'package:ogram/src/resources/database/saveLocal.dart';
import 'package:intl/intl.dart';

class OgramUpload extends StatefulWidget {
  @override
  _OgramUpload createState() => _OgramUpload();
}

class _OgramUpload extends State<OgramUpload> {
  String status = '';
  String base64Image;
  String errmsg = 'Error when uploading...';
  Future<File> file;
  File tmpPicture;

  selectPicture() async {
    setState(() {
      file = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }

  setStatus(String str) {
    setState(() {
      status = str;
    });
  }

  startUpload() {
    setStatus('Uploading image, please wait...');
    if (tmpPicture == null) {
      setStatus(errmsg);
      return;
    }
    String fileName = tmpPicture.path.split('/').last;
    upload(fileName, tmpPicture);
  }

  upload(String str, File file) async {
    String userID = await saveLocal.getStringId();
    Response response;
    Dio dio = new Dio();
    FormData formData = new FormData.fromMap({
      "chat_id": userID,
      "file": await MultipartFile.fromFile(tmpPicture.path, filename: str)
    });
    await dio
        .post("https://ogramcloud.com/api/upload", data: formData)
        .then((response) async {
      setStatus(response.statusCode == 200 ? 'Upload is success' : errmsg);
      var dateT = json.decode(response.toString())['json_map']['cloud_map'][0]
          ['datetime'];
      final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('yyyy-MM-dd');
      final String formatted = formatter.format(now);
      final database =
          await $FloorAppDatabase.databaseBuilder('app_database.db').build();
      final ogramFileDao = database.ogramFileDao;
      await ogramFileDao
          .insertOgramFile(new OgramFile(
              null,
              str,
              'https://ogramcloud.com/api/file/' +
                  json.decode(response.toString())['file_key'],
              formatted))
          .then((_) {
        homeBloc.getLisOgram();
      });
    }).catchError((onError) {
      print(onError);
      setStatus(onError.toString());
    });
  }

  Widget showPicture() {
    return FutureBuilder<File>(
        future: file,
        builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            tmpPicture = snapshot.data;
            base64Image = base64Encode(snapshot.data.readAsBytesSync());
            return ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 70,
                  minHeight: 70,
                  maxWidth: 150,
                  maxHeight: 150,
                ),
                child: Row(
                  children: [
                    Expanded(
                        child: Image.file(snapshot.data, fit: BoxFit.cover))
                  ],
                ));
          } else if (snapshot.error != null) {
            return Text(
              'Error picking Picture',
              textAlign: TextAlign.center,
            );
          } else {
            return Text(
              'No Picture selected',
              textAlign: TextAlign.center,
            );
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          showPicture(),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              status,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.blue,
                  fontSize: 20.0,
                  fontFamily: 'quicksand'),
            ),
          ),
          FutureBuilder<File>(
              future: file,
              builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.data != null) {
                  return status == 'Upload is success'
                      ? Padding(
                          padding: EdgeInsets.only(
                              top: 10, bottom: 5, left: 20, right: 20),
                          child: RaisedButton(
                            padding: EdgeInsets.all(10),
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.blue)),
                            onPressed: selectPicture,
                            color: Colors.white,
                            textColor: Colors.white,
                            child: Text(
                                "select another your image".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 14, color: Colors.blue)),
                          ))
                      : Padding(
                          padding: EdgeInsets.only(
                              top: 10, bottom: 5, left: 20, right: 20),
                          child: RaisedButton(
                            padding: EdgeInsets.all(10),
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.blue)),
                            onPressed: startUpload,
                            color: Colors.white,
                            textColor: Colors.white,
                            child: Text("upload your image".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 14, color: Colors.blue)),
                          ));
                } else {
                  return Padding(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 5, left: 20, right: 20),
                      child: RaisedButton(
                        padding: EdgeInsets.all(10),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.blue)),
                        onPressed: selectPicture,
                        color: Colors.white,
                        textColor: Colors.white,
                        child: Text("select your image".toUpperCase(),
                            style: TextStyle(fontSize: 14, color: Colors.blue)),
                      ));
                }
              })
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ogram/src/blocs/route_bloc.dart';
import 'package:ogram/src/ui/bodyMain.dart';
import 'package:ogram/src/ui/obtainId_ui.dart';
import 'blocs/auth_bloc.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    auth_bloc.restoreSession();
    route_bloc.levelPage();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StreamBuilder<bool>(
          stream: auth_bloc.isSession,
          builder: (context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData && snapshot.data)
              return BodyMain();
            else
              return ObtainIdUI();
          }),
    );
  }
}

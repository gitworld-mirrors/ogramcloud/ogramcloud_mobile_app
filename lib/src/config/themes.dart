import 'package:flutter/material.dart';

class AppSize {
  static const double h1 = 24.0;
  static const double textbutton = 16.0;
}

class AppColors {
  static const blue = Color(0xff0069ff);
  static const bluelight = Color(0xff006aff94);
  static const white = Color(0xffffffff);
}

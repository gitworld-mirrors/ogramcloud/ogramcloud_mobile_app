// entity/ogramfile.dart

import 'package:floor/floor.dart';

@entity
class OgramFile {
  @primaryKey
  final int id;

  final String name;

  final String link;

  final String date;

  OgramFile(this.id, this.name, this.link, this.date);
}

//DAO

@dao
abstract class OgramFileDao {
  @Query('SELECT * FROM OgramFile ORDER BY id DESC')
  Future<List<OgramFile>> findAllOgramFiles();

  @Query('SELECT * FROM OgramFile WHERE id = :id')
  Stream<OgramFile> findOgramFileById(int id);

  @insert
  Future<void> insertOgramFile(OgramFile ogramFile);
}

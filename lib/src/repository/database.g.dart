// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  OgramFileDao _ogramFileDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `OgramFile` (`id` INTEGER, `name` TEXT, `link` TEXT, `date` TEXT, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  OgramFileDao get ogramFileDao {
    return _ogramFileDaoInstance ??= _$OgramFileDao(database, changeListener);
  }
}

class _$OgramFileDao extends OgramFileDao {
  _$OgramFileDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _ogramFileInsertionAdapter = InsertionAdapter(
            database,
            'OgramFile',
            (OgramFile item) => <String, dynamic>{
                  'id': item.id,
                  'name': item.name,
                  'link': item.link,
                  'date': item.date
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _ogramFileMapper = (Map<String, dynamic> row) => OgramFile(
      row['id'] as int,
      row['name'] as String,
      row['link'] as String,
      row['date'] as String);

  final InsertionAdapter<OgramFile> _ogramFileInsertionAdapter;

  @override
  Future<List<OgramFile>> findAllOgramFiles() async {
    return _queryAdapter.queryList('SELECT * FROM OgramFile ORDER BY id DESC',
        mapper: _ogramFileMapper);
  }

  @override
  Stream<OgramFile> findOgramFileById(int id) {
    return _queryAdapter.queryStream('SELECT * FROM OgramFile WHERE id = ?',
        arguments: <dynamic>[id],
        queryableName: 'OgramFile',
        isView: false,
        mapper: _ogramFileMapper);
  }

  @override
  Future<void> insertOgramFile(OgramFile ogramFile) async {
    await _ogramFileInsertionAdapter.insert(
        ogramFile, OnConflictStrategy.abort);
  }
}

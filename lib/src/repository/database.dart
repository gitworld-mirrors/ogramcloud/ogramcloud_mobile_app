import 'dart:async';
import 'package:floor/floor.dart';
import 'package:ogram/src/repository/dao/ogramfile_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [OgramFile])
abstract class AppDatabase extends FloorDatabase {
  OgramFileDao get ogramFileDao;
}

import 'package:ogram/src/config/routes.dart';
import 'package:rxdart/rxdart.dart';

class RouteBloc {
  final PublishSubject _pageName = PublishSubject<String>();
  Stream<String> get pageName => _pageName.stream;

  void dispose() {
    _pageName.close();
  }

  void levelPage() async {
    _pageName.sink.add(OgramRoute.home);
    print(OgramRoute.home);
  }

  void switchPage() async {
    _pageName.sink.add(OgramRoute.upload);
  }
}

final route_bloc = RouteBloc();

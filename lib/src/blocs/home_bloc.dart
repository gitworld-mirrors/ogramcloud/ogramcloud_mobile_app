import 'package:ogram/src/repository/dao/ogramfile_dao.dart';
import 'package:ogram/src/repository/database.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeBloc {
  final PublishSubject _idUser = PublishSubject<String>();
  final PublishSubject _listOgramFile = PublishSubject<List<OgramFile>>();
  Stream<String> get idUser => _idUser.stream;
  Stream<List<OgramFile>> get listOgram => _listOgramFile.stream;

  init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _idUser.sink.add(prefs.getString("iduser"));
    getLisOgram();
  }

  getLisOgram() async {
    final database =
        await $FloorAppDatabase.databaseBuilder('app_database.db').build();
    final ogramFileDao = database.ogramFileDao;
    await ogramFileDao.findAllOgramFiles().then((value) {
      _listOgramFile.sink.add(value);
    });
  }
}

final homeBloc = HomeBloc();

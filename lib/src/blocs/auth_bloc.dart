import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthBlock {
  final PublishSubject _isSession = PublishSubject<bool>();
  Stream<bool> get isSession => _isSession.stream;

  void dispose() {
    _isSession.close();
  }

  void openSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("jalon", "mdrOgramCloud");
    _isSession.sink.add(true);
  }

  void restoreSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.get("jalon") != null && prefs.get("jalon").length > 0) {
      _isSession.sink.add(true);
    } else {
      _isSession.sink.add(false);
    }
  }

  void closeSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("jalon");
    _isSession.sink.add(false);
  }
}

final auth_bloc = AuthBlock();

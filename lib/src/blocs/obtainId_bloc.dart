import 'package:ogram/src/resources/database/saveLocal.dart';
import 'package:ogram/src/utilities/validator.dart';
import 'package:rxdart/rxdart.dart';

import 'auth_bloc.dart';

class ObtainIdBloc extends Validators {
  final BehaviorSubject _idController = BehaviorSubject<String>();
  final PublishSubject _loadingSaveData = PublishSubject<bool>();

  Function(String) get changeId => _idController.sink.add;
  Stream<String> get id => _idController.stream.transform(validateId);
  Stream<bool> get loading => _loadingSaveData.stream;
  Stream<bool> get isSubmit => Rx.combineLatest([id], (id) => true);

  void submit() {
    final id = _idController.value;
    _loadingSaveData.sink.add(true);
    saveID(id);
  }

  saveID(String id) async {
    bool t = await saveLocal.register(id);
    _loadingSaveData.sink.add(false);
    auth_bloc.openSession();
  }
}

import 'package:shared_preferences/shared_preferences.dart';

class SaveLocal {
  Future<bool> register(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("iduser", id);
    await Future.delayed(Duration(seconds: 3));
    return true;
  }

  Future<String> getStringId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('iduser');
  }
}

final saveLocal = SaveLocal();
